//┌────────────────────────────────────────────────────────────────────────────┐
//│ PasteIt - A paste client                                                   │
//│ ========================                                                   │
//│ File   : pasteit/src/main.rs                                               │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/pasteit                            │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sat, 8th January 2022 4:20:27 PM -06:00                     │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

mod commandline {
    use clap::{App, Arg, ArgMatches, ArgGroup};
    use pasteit::{self, *};
    use url::Url;

    struct GlobalArguments {
        pub json_flag: bool,
        pub plain_flag: bool,
        pub debug_flag: bool,
        pub no_qr_code_flag: bool,
        pub url: Url,
    }

    #[derive(Debug, Clone)]
    struct PasteSource<'a> {
        optional_filepath: Option<&'a str>,
    }

    impl<'a> PasteSource<'a> {
        pub fn new<'b: 'a>(optional_filepath: Option<&'b str>) -> Self {
            PasteSource { optional_filepath }
        }

        pub fn into_text(self) -> String {
            use std::{fs, io, io::Read};

            let mut paste_text = String::from("");
            if let Some(filepath) = self.optional_filepath {
                paste_text = fs::read_to_string(filepath).unwrap();
            } else {
                io::stdin().read_to_string(&mut paste_text).unwrap();
            }

            return paste_text;
        }
    }

    #[derive(Debug, Clone)]
    struct CommentSource<'a> {
        optional_text: Option<&'a str>,
    }

    impl<'a> CommentSource<'a> {
        pub fn new<'b: 'a>(optional_text: Option<&'b str>) -> Self {
            CommentSource { optional_text }
        }

        pub fn into_text(self) -> String {
            use std::{io, io::Read};

            let mut comment_text = String::from("");
            if let Some(text) = self.optional_text {
                comment_text = text.into();
            } else {
                io::stdin().read_to_string(&mut comment_text).unwrap();
            }

            return comment_text;
        }
    }

    pub fn retrieve_all_arguments() -> ArgMatches {
        clap::app_from_crate!()

            .groups(&[
                ArgGroup::new("create_replace")
                    .conflicts_with("delete")
                    .multiple(true)
                    .required(false),
                ArgGroup::new("create_paste")
                    .conflicts_with_all(&["comment", "delete"])
                    .multiple(true)
                    .required(false),
                ArgGroup::new("create_paste_filepath")
                    .conflicts_with_all(&["create", "replace", "comment", "delete"])
                    .multiple(true)
                    .required(false),
            ])

            // Global options

            .args(&[
                Arg::new("json")
                    .help("Output in the JSON format for machine readability and scripting purposes.")
                    .long("json")
                    .takes_value(false)
                    .global(true),
                Arg::new("plain")
                    .help("Output plain text of the output without extra information, for machine readability and scripting purposes.")
                    .long("plain")
                    .takes_value(false)
                    .global(true),
                Arg::new("debug")
                    .help("Output debug messages.")
                    .long("debug")
                    .takes_value(false)
                    .global(true),

                // `create` and `replace` options (when subcommand is not `delete`)
                // To be in the group `create_replace` to conflict with `delete`

                Arg::new("url")
                    .help("Url of the paste website. Please refer to https://privatebin.info/directory/ for a list of privatebin instances.")
                    .long("url")
                    .short('u')
                    .takes_value(true)
                    .default_value("https://paste.rebornos.org")
                    .forbid_empty_values(true)
                    .global(true)
                    .group("create_replace"),
                Arg::new("password")
                    .help("A password to access the paste.")
                    .long("password")
                    .short('p')
                    .takes_value(true)
                    .default_value("")
                    .forbid_empty_values(false)
                    .global(true)
                    .group("create_replace"),

                Arg::new("compression_type")
                    .help("Compression type.")
                    .long("compression-type")
                    .short('c')
                    .takes_value(true)
                    .possible_values(["zlib", "none"])
                    .default_value("zlib")
                    .global(true)
                    .group("create_replace"),
                Arg::new("cipher_algorithm")
                    .help("Cipher algorithm.")
                    .long("cipher-algorithm")
                    .short('a')
                    .takes_value(true)
                    .possible_values(["aes"])
                    .default_value("aes")
                    .global(true)
                    .group("create_replace"),
                Arg::new("cipher_mode")
                    .help("Cipher mode.")
                    .long("cipher-mode")
                    .short('m')
                    .takes_value(true)
                    .possible_values(["gcm"])
                    .default_value("gcm")
                    .global(true)
                    .group("create_replace"),

                Arg::new("no_qr_code")
                    .help("Do not display a QR code to access the paste/comment.")
                    .long("no-qr")
                    .short('q')
                    .takes_value(false)
                    .global(true)
                    .group("create_replace"),

                // Global filepath no subcommand is specified
                // To be in the group create_paste_filepath to conflict with `[create, replace, delete]`

                Arg::new("filepath")
                    .help("Absolute or relative path of the file to upload for the paste. If not provided, a string is read from STDIN. Not applicable for comments.")
                    .global(true)
                    .group("create_paste_filepath"),

                // `create paste` global options  
                // To be in the group `create_paste` to conflict with `[comment, delete]`

                Arg::new("expiry_duration")
                    .help("Expiry time for the paste. The expiry time can be specified in plain words, like 5 min, 10 days, 50s, 1 month, or never.")
                    .long("expiry-duration")
                    .short('e')
                    .takes_value(true)
                    .default_value("1day")
                    .global(true)
                    .group("create_paste"),
                Arg::new("burn_after_reading")
                    .help("If enabled, deletes the paste after opening once.")
                    .long("burn-after-reading")
                    .short('b')
                    .takes_value(false)
                    .global(true)
                    .group("create_paste"),
                Arg::new("open_discussion")
                    .help("If enabled, permits comments on the paste page.")
                    .long("open-discussion")
                    .short('o')
                    .takes_value(false)
                    .global(true)
                    .group("create_paste"),
                Arg::new("display_format")
                    .help("Paste display format.")
                    .long("display-format")
                    .short('f')
                    .takes_value(true)
                    .possible_values(["plaintext", "syntaxhighlighting", "markdown"])
                    .default_value("plaintext")
                    .global(true)
                    .group("create_paste"),               
            ])

            .subcommand(App::new("create")
                .about("Control creating of pastes and comments.")
                .subcommands([

                    App::new("paste")
                        .about("Create a new paste")
                        .arg(
                            // Some other arguments handled by global options in the group `create_replace` which conflicts with `[delete]`
                            // Some other arguments handled by global options in the group `create_paste` which conflicts with `[comment, delete]`
                            Arg::new("filepath")
                                .help("Absolute or relative path of the file to upload. If not provided, a string is read from STDIN.")
                                .groups(&["create", "paste"])
                        ),

                    App::new("comment")
                        .about("Create a new comment")
                        .args(&[
                            Arg::new("text")
                                .help("The comment text. If not provided, a string is read from STDIN.")
                                .groups(&["create", "comment"]),
                            Arg::new("paste_id")
                                .help("The ID of the paste to comment on.")
                                .long("paste-id")
                                .short('i')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(true)
                                .groups(&["create", "comment"]),
                            Arg::new("decryption_key")
                                .help("The decryption key of the paste.")
                                .long("key")
                                .short('k')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(true)
                                .groups(&["create", "comment"]),
                            Arg::new("parent_id")
                                .help("The ID of the parent paste/comment to comment on.")
                                .long("parent-id")
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(false)
                                .groups(&["create", "comment"]),
                            Arg::new("nickname")
                                .help("An optional nickname to comment as.")
                                .long("nickname")
                                .short('n')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(false)
                                .groups(&["create", "comment"]),
                        ]),                        
                ])                
            )

            .subcommand(App::new("replace")
                .about("Control replacing of pastes and comments.") 
                .subcommands([
                    App::new("paste")
                        .about("Replace a paste")
                        .args(&[
                            // Some other arguments handled by global options in the group `create_replace` which conflicts with `[delete]`
                            // Some other arguments handled by global options in the group `create_paste` which conflicts with `[comment, delete]`
                            Arg::new("filepath")
                                .help("Absolute or relative path of the file to upload. If not provided, a string is read from STDIN.")
                                .groups(&["replace", "paste"]),
                            Arg::new("paste_id")
                                .help("The ID of the paste to replace.")
                                .long("paste-id")
                                .short('i')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(true)
                                .groups(&["replace", "paste"]),                            
                        ]),
                        
                    App::new("comment")
                        .about("Replace a comment")
                        .args(&[
                            // Some other arguments handled by global options in the group `create_replace` which conflicts with `[delete]`
                            Arg::new("text")
                                .help("The comment text. If not provided, a string is read from STDIN.")
                                .groups(&["replace", "comment"]),
                            Arg::new("paste_id")
                                .help("The ID of the paste to comment on.")
                                .long("paste-id")
                                .short('i')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(true)
                                .groups(&["replace", "comment"]),
                            Arg::new("decryption_key")
                                .help("The decryption key of the paste.")
                                .long("key")
                                .short('k')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(true)
                                .groups(&["replace", "comment"]),
                            Arg::new("parent_id")
                                .help("The ID of the parent paste/comment to comment on.")
                                .long("parent-id")
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(false)
                                .groups(&["replace", "comment"]),
                            Arg::new("nickname")
                                .help("An optional nickname to comment as.")
                                .long("nickname")
                                .short('n')
                                .takes_value(true)
                                .forbid_empty_values(true)
                                .required(false)
                                .groups(&["replace", "comment"]),
                        ]),
                ])                 
            )
            .subcommand(App::new("delete")
                .about("Delete a paste.") 
                .args(&[
                    Arg::new("paste_id")
                        .help("The ID of the paste to delete.")
                        .long("paste-id")
                        .short('i')
                        .takes_value(true)
                        .forbid_empty_values(true)
                        .required(true)                        
                        .group("delete"),
                    Arg::new("delete_token")
                        .help("The delete token of the paste.")
                        .long("delete-token")
                        .short('t')
                        .takes_value(true)
                        .forbid_empty_values(true)
                        .required(true)                        
                        .group("delete")
                ])               
            )
            .get_matches()
    }

    pub async fn process_all_arguments<'a>(matches: &ArgMatches) -> Result<(), Error> {
        let global_arguments = GlobalArguments {
            json_flag: matches.is_present("json"),
            plain_flag: matches.is_present("plain"),
            debug_flag: matches.is_present("debug"),
            no_qr_code_flag: matches.is_present("no_qr_code"),
            url: matches
                .value_of("url")
                .unwrap()
                .try_into()
                .unwrap_or(url::Url::parse("https://paste.rebornos.org").unwrap()),
        };

        if let None = matches.subcommand_name() {
            create_paste(matches, global_arguments).await?;
        } else if let Some(matches) = matches.subcommand_matches("create") {
            if let Some(matches) = matches.subcommand_matches("paste") {
                create_paste(matches, global_arguments).await?;
            } else if let Some(matches) = matches.subcommand_matches("comment") {
                create_comment(matches, global_arguments).await?;
            }
        } else {
            eprintln!("Error: Subcommand not supported");
            std::process::exit(1);
        }
        Ok(())
    }

    fn paste_input_interface(paste_arguments: &ArgMatches) -> Result<PasteInputInterface, Error> {
        Ok(PasteInputInterface {
            paste_text: PasteSource::new(paste_arguments.value_of("filepath")).into_text(),
            expiry_duration: paste_arguments.value_of("expiry_duration").unwrap().into(),
            burn_after_reading: paste_arguments.is_present("burn_after_reading"),
            open_discussion: paste_arguments.is_present("open_discussion"),
            paste_display_format: paste_arguments
                .value_of("display_format")
                .unwrap()
                .to_string()
                .try_into()?,
            url: paste_arguments
                .value_of("url")
                .unwrap()
                .try_into()
                .unwrap_or(url::Url::parse("https://paste.rebornos.org").unwrap()),
            password: paste_arguments.value_of("password").unwrap().into(),
            compression_type: paste_arguments
                .value_of("compression_type")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_algorithm: paste_arguments
                .value_of("cipher_algorithm")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_mode: paste_arguments
                .value_of("cipher_mode")
                .unwrap()
                .to_string()
                .try_into()?,
            id: paste_arguments.value_of("expiry_duration").map(Into::into),
        })
    }

    fn comment_input_interface(
        comment_arguments: &ArgMatches,
    ) -> Result<CommentInputInterface, Error> {
        Ok(CommentInputInterface {
            comment_text: CommentSource::new(comment_arguments.value_of("text")).into_text(),
            url: comment_arguments
                .value_of("url")
                .unwrap()
                .try_into()
                .unwrap_or(url::Url::parse("https://paste.rebornos.org").unwrap()),
            password: comment_arguments.value_of("password").unwrap().into(),
            compression_type: comment_arguments
                .value_of("compression_type")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_algorithm: comment_arguments
                .value_of("cipher_algorithm")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_mode: comment_arguments
                .value_of("cipher_mode")
                .unwrap()
                .to_string()
                .try_into()?,
            paste_id: comment_arguments.value_of("paste_id").unwrap().into(),
            parent_id: comment_arguments
                .value_of("paste_id")
                .unwrap_or(comment_arguments.value_of("paste_id").unwrap())
                .into(),
            nickname: comment_arguments.value_of("nickname").unwrap().into(),
            decryption_key: comment_arguments.value_of("decryption_key").unwrap().into(),
        })
    }

    async fn create_paste(
        paste_arguments: &ArgMatches,
        global_arguments: GlobalArguments,
    ) -> Result<(), Error> {
        let paste_output_interface = privatebin::create_paste(
            &mut paste_input_interface(paste_arguments)?,
            global_arguments.debug_flag,
        )
        .await?;
        if ! global_arguments.json_flag && ! global_arguments.plain_flag {
            use owo_colors::{OwoColorize, Stream::Stdout};
            println!(
                "{}",
                "Paste creation successful...".if_supports_color(Stdout, |text| text.green())
            );
            println!(
                "{}: {}",
                "Paste URL".if_supports_color(Stdout, |text| text.magenta()),
                paste_output_interface.url
            );
            println!(
                "{}: {}",
                "Paste ID".if_supports_color(Stdout, |text| text.magenta()),
                paste_output_interface.paste_id
            );
            println!(
                "{}: {}",
                "Decryption Key".if_supports_color(Stdout, |text| text.magenta()),
                paste_output_interface.decryption_key
            );
            println!(
                "{}: {}",
                "Deletion URL".if_supports_color(Stdout, |text| text.magenta()),
                paste_output_interface.deletion_url
            );
            println!(
                "{}: {}",
                "Deletion token".if_supports_color(Stdout, |text| text.magenta()),
                paste_output_interface.deletion_token
            );

            if ! global_arguments.no_qr_code_flag && ! global_arguments.plain_flag && ! global_arguments.json_flag {
                use std::process::Command;
                Command::new("qrencode").arg("-t").arg("UTF8").arg::<String>(paste_output_interface.url.into()).status().expect("Error while running `qrencode`. Please check if the application is installed.");
            }
        } else if global_arguments.json_flag {
            println!(
                "{}",
                serde_json::to_string(&paste_output_interface).unwrap()
            );
        } else if global_arguments.plain_flag {
            println!(
                "{}",
                paste_output_interface.url
            );
        }
        Ok(())
    }

    async fn create_comment(
        comment_arguments: &ArgMatches,
        global_arguments: GlobalArguments,
    ) -> Result<(), Error> {
        let comment_output_interface = privatebin::create_comment(
            &mut comment_input_interface(comment_arguments)?,
            global_arguments.debug_flag,
        )
        .await?;
        if ! global_arguments.json_flag && ! global_arguments.plain_flag {
            use owo_colors::{OwoColorize, Stream::Stdout};
            println!("Comment creation successful...").if_supports_color(Stdout, |text| text.green());
            println!("{}: {}", "Comment URL".if_supports_color(Stdout, |text| text.magenta()), comment_output_interface.url);
            println!("{}: {}", "Comment ID".if_supports_color(Stdout, |text| text.magenta()), comment_output_interface.comment_id);

            if ! global_arguments.no_qr_code_flag && ! global_arguments.plain_flag && ! global_arguments.json_flag {
                use std::process::Command;
                Command::new("qrencode").arg("-t").arg("UTF8").arg::<String>(comment_output_interface.url.into()).status().expect("Error while running `qrencode`. Please check if the application is installed.");
            }
        } else if global_arguments.json_flag {
            println!(
                "{}",
                serde_json::to_string(&comment_output_interface).unwrap()
            );
        } else if global_arguments.plain_flag {
            println!(
                "{}",
                comment_output_interface.url
            );
        }
        Ok(())
    }

    // pub async fn handle_options(options: Options) -> Result<(), sdart::paste::Error> {
    //     use sdart::paste::*;
    //     match options {
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Create {
    //                     create_type:
    //                         CreateType::Paste {
    //                             filepath,
    //                             password,
    //                             url,
    //                             expiry_duration,
    //                             burn_after_reading,
    //                             open_discussion,
    //                             paste_display_format,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                             qr_code,
    //                         },
    //                 },
    //         } => {
    //             let paste_text = text_from_filepath_or_stdin(filepath)?;
    //             let paste_output_interface = create_paste(&mut PasteInputInterface {
    //                 paste_text,
    //                 password,
    //                 url,
    //                 id: None,
    //                 expiry_duration,
    //                 burn_after_reading,
    //                 open_discussion,
    //                 paste_display_format: paste_display_format.try_into()?,
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 use owo_colors::{OwoColorize, Stream::Stdout};
    //                 println!("{}", "Paste creation successful...".if_supports_color(Stdout, |text| text.bright_blue()));
    //                 println!("{}: {}", "Paste URL".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.url);
    //                 println!("{}: {}", "Deletion URL".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.deletion_url);
    //                 println!("{}: {}", "Paste ID".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.paste_id);
    //                 println!("{}: {}", "Deletion token".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.deletion_token);

    //                 if qr_code {
    //                     use std::process::Command;
    //                     Command::new("qrencode").arg("-t").arg("UTF8").arg::<String>(paste_output_interface.url.into()).status().expect("Error while running `qrencode`. Please check if the application is installed.");
    //                 }
    //             } else {
    //                 println!("{}", serde_json::to_string(&paste_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Create {
    //                     create_type:
    //                         CreateType::Comment {
    //                             comment_text,
    //                             password,
    //                             url,
    //                             paste_id,
    //                             parent_id,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                         },
    //                 },
    //         } => {
    //             let comment_output_interface = create_comment(&mut CommentInputInterface {
    //                 comment_text,
    //                 password,
    //                 url,
    //                 paste_id: paste_id.clone(),
    //                 parent_id: parent_id.unwrap_or(paste_id),
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Comment creation successful...");
    //                 println!("Comment URL: {}", comment_output_interface.url);
    //                 println!("Comment ID: {}", comment_output_interface.comment_id);
    //             } else {
    //                 println!("{}", serde_json::to_string(&comment_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Replace {
    //                     replace_type:
    //                         ReplaceType::Paste {
    //                             filepath,
    //                             password,
    //                             url,
    //                             paste_id,
    //                             expiry_duration,
    //                             burn_after_reading,
    //                             open_discussion,
    //                             paste_display_format,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                         },
    //                 },
    //         } => {
    //             let paste_text = text_from_filepath_or_stdin(filepath)?;
    //             let paste_output_interface = replace_paste(&mut PasteInputInterface {
    //                 paste_text,
    //                 password,
    //                 url,
    //                 id: Some(paste_id),
    //                 expiry_duration,
    //                 burn_after_reading,
    //                 open_discussion,
    //                 paste_display_format: paste_display_format.try_into()?,
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Paste replacement successful...");
    //                 println!("Paste URL: {}", paste_output_interface.url);
    //                 println!("Deletion URL: {}", paste_output_interface.deletion_url);
    //                 println!("Paste ID: {}", paste_output_interface.paste_id);
    //                 println!("Deletion token: {}", paste_output_interface.deletion_token);
    //             } else {
    //                 println!("{}", serde_json::to_string(&paste_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Replace {
    //                     replace_type:
    //                         ReplaceType::Comment {
    //                             comment_text,
    //                             password,
    //                             url,
    //                             comment_id,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                         },
    //                 },
    //         } => {
    //             let comment_output_interface = replace_comment(&mut CommentInputInterface {
    //                 comment_text,
    //                 password,
    //                 url,
    //                 paste_id: comment_id.clone(),
    //                 parent_id: comment_id.clone(),
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Comment replacement successful...");
    //                 println!("Comment URL: {}", comment_output_interface.url);
    //                 println!("Comment ID: {}", comment_output_interface.comment_id);
    //             } else {
    //                 println!("{}", serde_json::to_string(&comment_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Delete {
    //                     url,
    //                     paste_id,
    //                     delete_token,
    //                 },
    //         } => {
    //             delete_paste(&mut DeletionInputInterface {
    //                 paste_id,
    //                 delete_token,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Paste deletion successful...");
    //             } else {
    //                 println!("{}", serde_json::to_string(&DeletionOutputInterface{}).unwrap());
    //             }
    //         }
    //     };

    //     Ok(())
    // }
}

#[tokio::main]
async fn main() {
    let matches = commandline::retrieve_all_arguments();
    if let Err(error) = commandline::process_all_arguments(&matches).await {
        eprintln!("Error: {}", error);
        std::process::exit(1);
    }
}
