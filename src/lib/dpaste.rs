//┌────────────────────────────────────────────────────────────────────────────┐
//│ PasteIt - A paste client                                                   │
//│ ========================                                                   │
//│ File   : pasteit/src/lib/dpaste.rs                                         │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/pasteit                            │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sat, 8th January 2022 4:19:54 PM -06:00                     │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

