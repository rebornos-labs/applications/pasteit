# PasteIt - A paste client

*Authors*
1. *shivanandvp@rebornos.org*


## Status

This project is in the *Pre-Alpha* stage. Use at your own risk. Currently **pasteit** only supports [PrivateBin](https://privatebin.info/) instances.


## Introduction

**PasteIt** can be used to upload a file or text to a *paste* server like [PrivateBin](https://privatebin.info/).
In addition to that, **pasteit** can post comments,delete posts or comments, and adjust various different options on supported paste servers.

<!-- [[_TOC_]] -->

## Documentation

### API

[CLICK HERE](https://shivanandvp.gitlab.io/pasteit/pasteit/index.html) for the API documentation of pasteit.

### Usage 

> Note: At any time, call `pasteit` with the `--help` option to see the allowed arguments.



## License

This project is licensed under the [Mozilla Public License (MPL-2.0)](https://www.mozilla.org/en-US/MPL/2.0/)

### ToDo List

Here is a high-level list of planned features
 
- [X] PrivateBin support
  - [x] Post 
    - [x] Post Paste
    - [x] Post Options
    - [x] Post Comment
  - [ ] Delete
    - [ ] Delete Paste
    - [ ] Delete Comment
  - [ ] Edit Posts
    - [ ] Edit Comments
- [ ] dpaste.com support
    - [ ] Post Paste
- [x] QR code support